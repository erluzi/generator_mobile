var webpack = require('webpack');
var HtmlWebpackPlugin = require('html-webpack-plugin');
var ExtractTextPlugin = require('extract-text-webpack-plugin');
var config = require('./webpack.base.js');

config.output.filename = '[name].[hash].js';
config.plugins = [
	new ExtractTextPlugin("[name].[hash].css",{allChunks: true, resolve: ['modules']}),
	new webpack.optimize.CommonsChunkPlugin('vendors', 'vendors.[hash].js'),
	new webpack.DefinePlugin({
		'process.env': {
			NODE_ENV: '"production"'
		}
	}),
	new webpack.optimize.UglifyJsPlugin({
		compress: {
			warnings: false
		},
		except: ['$', 'exports', 'require']
	}),
	new HtmlWebpackPlugin({
		filename: '../index.html', //相对output.path
		template: './react/template/index.html',
		inject: 'body'
	})
];

module.exports = config;