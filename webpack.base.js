var path = require('path');
var ExtractTextPlugin = require('extract-text-webpack-plugin');
module.exports = {
	entry: {
		index: './react/index.js',
		vendors: ['react', 'react-dom', 'react-router']
	},
	output: {
		path: path.resolve(__dirname,'./dist/app/wap'),
		publicPath: '/app/wap/'
	},
	module: {
		loaders: [
			{
				test: /\.js$/,
				exclude: /node_modules/,
				loader: 'babel-loader?presets[]=es2015&presets[]=react'
			},
			{
				test: /\.css$/,
				loader: ExtractTextPlugin.extract(
					'css'
				)
			},
			{
				test: /\.less/,
				loader: ExtractTextPlugin.extract(
					'css!autoprefixer?{browsers: ["last 2 version", "> 5%"]}!less' //path默认为output.path
				)
			},
			{
				test: /\.(png|jpg|gif|woff|woff2)$/,
				loader: 'url-loader?limit=10000'
			}
		]
	}
};