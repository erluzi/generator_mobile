var webpack = require('webpack');
var HtmlWebpackPlugin = require('html-webpack-plugin');
var ExtractTextPlugin = require('extract-text-webpack-plugin');
var config = require('./webpack.base.js');

config.output.filename = '[name].js';
config.devtool = '#source-map';
config.plugins = [
	new ExtractTextPlugin("[name].css",{allChunks: true, resolve: ['modules']}),
	new webpack.optimize.CommonsChunkPlugin('vendors', 'vendors.js'),
	new HtmlWebpackPlugin({
		filename: '../index.html', //相对output.path
		template: './react/template/index.html',
		inject: 'body'
	})
];

module.exports = config;

