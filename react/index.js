import React from 'react'
import { render } from 'react-dom'
import {Router, browserHistory} from 'react-router'

import routes from './module/routes'

//render(<App/>, document.getElementById('app'))

render(
	(
	<Router routes={routes} history={browserHistory}/>
	//<Router history={browserHistory}>
	//	<Route path="/" component={App}>
	//		<IndexRoute component={Home} />
	//		<Route path="/about" component={About} />
	//		<Route path="/repos" component={Repos}>
	//			<Route path="/1" component={Repo1}>repos1</Route>
	//			<Route path="/2" component={Repo2}>repos2</Route>
	//			<Route path="/3" component={Repo3}>repos3</Route>
	//
	//			<Route path="/repos/:repoName" component={Repo}>repo</Route>
	//		</Route>
	//	</Route>
	//</Router>
	),
	document.getElementById('app')
);
