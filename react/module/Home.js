import React from 'react';

class Home extends React.Component {
	constructor(props) {
		super(props);
		this.state = {
			isLoading: true
		}
	}

	render() {
		if(this.state.isLoading){
			return (
				<div className="box-full">
					<div className="loading-spinner"></div>
				</div>
			)
		}else{
			return (
				<div>
					load over
				</div>
			)
		}
	}

	componentDidMount(){
		setTimeout(()=>{
			this.setState({
				isLoading: false
			})
		},3000)
	}
}

export default Home;
