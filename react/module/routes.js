///routes.js client/server 共用
import React from 'react'
import { Route, IndexRedirect } from 'react-router'

import App from './App'
import Home from './Home'

module.exports = (
	<Route path="/" component={App}>
		<IndexRedirect to="/share/home/1" />
		<Route path="share">
			<Route path="home">
				<Route path=":uid" component={Home} />
			</Route>
		</Route>
	</Route>
);